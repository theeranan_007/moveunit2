// Header.js
import Head from 'next/head'
const Header = () => {
    return (
        <div>
            <Head>
                <title>Learn NextJs</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
        </div>
    )
}
export default Header