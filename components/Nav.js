// Nav.js
import Link from 'next/link'
import { Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const PostLink = (props) => (
    <Link as={`/p/${props.id}`} href={`/?title=${props.title}`}>
      <a>{props.title}</a>
    </Link>
)
const Navbar = () => {
  return (
    <div >
      <Menu mode="horizontal" >
        <Menu.Item >
         <PostLink  id="hello-nextjs" title="Hello Next.js"/>
        </Menu.Item>
        <Menu.Item >
        <PostLink  id="abount" title="Abount Next.js"/>
        </Menu.Item>
      </Menu>
    </div>
  )
}
export default Navbar