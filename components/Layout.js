import { Layout } from 'antd';
import Head from './Header'
import Nav from './Nav'
import Index from './Sidebar'
import stylesheet from 'antd/dist/antd.css'
import { Menu, Icon, Breadcrumb } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const { Header, Footer, Sider, Content } = Layout;
const MyLayout = (props) => (
    <div>
        <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
        <Head />
        <Layout>
            <Header style={{ background: 'none', padding: 0 ,height:"100%"}} ><Nav /></Header>
            <Layout>
                <Content>{props.children}</Content>
                <Sider style={{background:"none"}}><Index/></Sider>
            </Layout>
        </Layout>
    </div>
)

export default MyLayout