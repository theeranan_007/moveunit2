import React, { Component } from 'react'
import Layout from '../components/Layout'
import Index from '../components/Sidebar'
const index = (props) => {
    return (
        <Layout>
            <h1>{props.url.query.title}</h1>
            <p>This is the blog post content.</p>
        </Layout>
    )
}
export default index;