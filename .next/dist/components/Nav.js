'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _link = require('next\\dist\\lib\\link.js');

var _link2 = _interopRequireDefault(_link);

var _antd = require('antd');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'D:\\xampp\\htdocs\\moveunit2\\components\\Nav.js';
// Nav.js

var SubMenu = _antd.Menu.SubMenu;
var MenuItemGroup = _antd.Menu.ItemGroup;
var PostLink = function PostLink(props) {
  return _react2.default.createElement(_link2.default, { as: '/p/' + props.id, href: '/?title=' + props.title, __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, _react2.default.createElement('a', {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, props.title));
};
var Navbar = function Navbar() {
  return _react2.default.createElement('div', {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    }
  }, _react2.default.createElement(_antd.Menu, { mode: 'horizontal', __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    }
  }, _react2.default.createElement(_antd.Menu.Item, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    }
  }, _react2.default.createElement(PostLink, { id: 'hello-nextjs', title: 'Hello Next.js', __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    }
  })), _react2.default.createElement(_antd.Menu.Item, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    }
  }, _react2.default.createElement(PostLink, { id: 'abount', title: 'Abount Next.js', __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    }
  }))));
};
exports.default = Navbar;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXE5hdi5qcyJdLCJuYW1lcyI6WyJMaW5rIiwiTWVudSIsIkljb24iLCJTdWJNZW51IiwiTWVudUl0ZW1Hcm91cCIsIkl0ZW1Hcm91cCIsIlBvc3RMaW5rIiwicHJvcHMiLCJpZCIsInRpdGxlIiwiTmF2YmFyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQ0EsQUFBTzs7OztBQUNQLEFBQVMsQUFBTTs7Ozs7QUFGZjs7QUFHQSxJQUFNLFVBQVUsV0FBaEIsQUFBcUI7QUFDckIsSUFBTSxnQkFBZ0IsV0FBdEIsQUFBMkI7QUFDM0IsSUFBTSxXQUFXLFNBQVgsQUFBVyxTQUFBLEFBQUMsT0FBRDt5QkFDYixBQUFDLGdDQUFLLFlBQVUsTUFBaEIsQUFBc0IsSUFBTSxtQkFBaUIsTUFBN0MsQUFBbUQ7Z0JBQW5EO2tCQUFBLEFBQ0U7QUFERjtHQUFBLGtCQUNFLGNBQUE7O2dCQUFBO2tCQUFBLEFBQUk7QUFBSjtBQUFBLFdBRlcsQUFDYixBQUNFLEFBQVU7QUFGaEI7QUFLQSxJQUFNLFNBQVMsU0FBVCxBQUFTLFNBQU0sQUFDbkI7eUJBQ0UsY0FBQTs7Z0JBQUE7a0JBQUEsQUFDRTtBQURGO0FBQUEsR0FBQSxrQkFDRSxBQUFDLDRCQUFLLE1BQU4sQUFBVztnQkFBWDtrQkFBQSxBQUNFO0FBREY7cUJBQ0csY0FBRCxXQUFBLEFBQU07O2dCQUFOO2tCQUFBLEFBQ0M7QUFERDtBQUFBLG1DQUNDLEFBQUMsWUFBVSxJQUFYLEFBQWMsZ0JBQWUsT0FBN0IsQUFBbUM7Z0JBQW5DO2tCQUZILEFBQ0UsQUFDQyxBQUVEO0FBRkM7dUJBRUEsY0FBRCxXQUFBLEFBQU07O2dCQUFOO2tCQUFBLEFBQ0E7QUFEQTtBQUFBLG1DQUNBLEFBQUMsWUFBVSxJQUFYLEFBQWMsVUFBUyxPQUF2QixBQUE2QjtnQkFBN0I7a0JBUE4sQUFDRSxBQUNFLEFBSUUsQUFDQSxBQUtQO0FBTE87O0FBUlIsQUFjQTtrQkFBQSxBQUFlIiwiZmlsZSI6Ik5hdi5qcyIsInNvdXJjZVJvb3QiOiJEOi94YW1wcC9odGRvY3MvbW92ZXVuaXQyIn0=