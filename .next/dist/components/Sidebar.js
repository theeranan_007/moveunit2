'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _antd = require('antd');

var _link = require('next\\dist\\lib\\link.js');

var _link2 = _interopRequireDefault(_link);

var _isomorphicUnfetch = require('isomorphic-unfetch');

var _isomorphicUnfetch2 = _interopRequireDefault(_isomorphicUnfetch);

var _Layout = require('../components/Layout');

var _Layout2 = _interopRequireDefault(_Layout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'D:\\xampp\\htdocs\\moveunit2\\components\\Sidebar.js';
// import React, { Component } from 'react'

var SubMenu = _antd.Menu.SubMenu;
var MenuItemGroup = _antd.Menu.ItemGroup;
// class Unit extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             show: ""
//         }
//     }
//     componentDidMount() {
//         asyncForEach()
//         async function asyncForEach() {
//             const res = await fetch('http://localhost:3000/api/data');
//             const data = await res.json()
//             console.log(`Show data fetched. Count: ${data.length}`)
//             alert(JSON.stringify(data))
//             this.setState({ show: data })
//         }
//     }
//     render() {
//         return (
//             <div>

//                 <Menu style={{ width: 256 }} mode="vertical">
//                     <SubMenu key="sub1" title={<span><Icon type="mail" /><span></span></span>}>
//                     </SubMenu>
//                 </Menu>
//             </div>
//         );
//     }
// }
var Unit = function Unit(props) {
    return _react2.default.createElement(_antd.Menu, { style: { width: 256 }, mode: 'vertical', __source: {
            fileName: _jsxFileName,
            lineNumber: 38
        }
    }, _react2.default.createElement(SubMenu, { key: 'sub1', title: _react2.default.createElement('span', {
            __source: {
                fileName: _jsxFileName,
                lineNumber: 39
            }
        }, _react2.default.createElement(_antd.Icon, { type: 'mail', __source: {
                fileName: _jsxFileName,
                lineNumber: 39
            }
        }), _react2.default.createElement('span', {
            __source: {
                fileName: _jsxFileName,
                lineNumber: 39
            }
        })), __source: {
            fileName: _jsxFileName,
            lineNumber: 39
        }
    }));
};
Unit.getInitialProps = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var res, data;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    _context.next = 2;
                    return (0, _isomorphicUnfetch2.default)('http://localhost:3000/api/data');

                case 2:
                    res = _context.sent;
                    _context.next = 5;
                    return res.json();

                case 5:
                    data = _context.sent;

                    console.log('Show data fetched. Count: ' + data.length);

                    alert((0, _stringify2.default)(data));
                    return _context.abrupt('return', {
                        shows: data
                    });

                case 9:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this);
}));
exports.default = Unit;
// const Index = (props) => (
//     <Menu style={{ width: 256 }} mode="vertical">
//     <SubMenu key="sub1" title={<span><Icon type="mail" /><span>Navigation One</span></span>}>
//       <MenuItemGroup title="Item 1">
//         <Menu.Item key="1">Option 1</Menu.Item>
//         <Menu.Item key="2">Option 2</Menu.Item>
//       </MenuItemGroup>
//       <MenuItemGroup title="Iteom 2">
//         <Menu.Item key="3">Option 3</Menu.Item>
//         <Menu.Item key="4">Option 4</Menu.Item>
//       </MenuItemGroup>
//     </SubMenu>
//     <SubMenu key="sub2" title={<span><Icon type="appstore" /><span>Navigation Two</span></span>}>
//       <Menu.Item key="5">Option 5</Menu.Item>
//       <Menu.Item key="6">Option 6</Menu.Item>
//       <SubMenu key="sub3" title="Submenu">
//         <Menu.Item key="7">Option 7</Menu.Item>
//         <Menu.Item key="8">Option 8</Menu.Item>
//       </SubMenu>
//     </SubMenu>
//     <SubMenu key="sub4" title={<span><Icon type="setting" /><span>Navigation Three</span></span>}>
//       <Menu.Item key="9">Option 9</Menu.Item>
//       <Menu.Item key="10">Option 10</Menu.Item>
//       <Menu.Item key="11">Option 11</Menu.Item>
//       <Menu.Item key="12">Option 12</Menu.Item>
//     </SubMenu>
//   </Menu>
// )

// Index.getInitialProps = async function() {
//   const res = await fetch('https://api.tvmaze.com/search/shows?q=batman')
//   const data = await res.json()

//   console.log(`Show data fetched. Count: ${data.length}`)
//   alert(JSON.stringify(data))
//   return {
//     shows: data
//   }
// }

// export default Index
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXFNpZGViYXIuanMiXSwibmFtZXMiOlsiTWVudSIsIkljb24iLCJMaW5rIiwiZmV0Y2giLCJMYXlvdXQiLCJTdWJNZW51IiwiTWVudUl0ZW1Hcm91cCIsIkl0ZW1Hcm91cCIsIlVuaXQiLCJwcm9wcyIsIndpZHRoIiwiZ2V0SW5pdGlhbFByb3BzIiwicmVzIiwianNvbiIsImRhdGEiLCJjb25zb2xlIiwibG9nIiwibGVuZ3RoIiwiYWxlcnQiLCJzaG93cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLEFBQVMsQUFBTTs7QUFDZixBQUFPOzs7O0FBQ1AsQUFBTzs7OztBQUNQLEFBQU8sQUFBWTs7Ozs7OztBQUpuQjs7QUFLQSxJQUFNLFVBQVUsV0FBaEIsQUFBcUI7QUFDckIsSUFBTSxnQkFBZ0IsV0FBdEIsQUFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBTSxPQUFPLFNBQVAsQUFBTyxLQUFBLEFBQUMsT0FBRDsyQkFDVCxBQUFDLDRCQUFLLE9BQU8sRUFBRSxPQUFmLEFBQWEsQUFBUyxPQUFPLE1BQTdCLEFBQWtDO3NCQUFsQzt3QkFBQSxBQUNJO0FBREo7S0FBQSxnQ0FDSSxBQUFDLFdBQVEsS0FBVCxBQUFhLFFBQU8sdUJBQU8sY0FBQTs7MEJBQUE7NEJBQUEsQUFBTTtBQUFOO0FBQUEsU0FBQSxrQkFBTSxBQUFDLDRCQUFLLE1BQU4sQUFBVzswQkFBWDs0QkFBTixBQUFNLEFBQW9CO0FBQXBCOzs7MEJBQW9COzRCQUFyRCxBQUEyQixBQUEwQjtBQUFBO0FBQUE7c0JBQXJEO3dCQUZLLEFBQ1QsQUFDSTtBQUFBOztBQUZSO0FBTUEsS0FBQSxBQUFLLDJGQUFrQixtQkFBQTthQUFBO2tFQUFBO2tCQUFBOzZDQUFBO3FCQUFBO29DQUFBOzJCQUNELGlDQURDLEFBQ0QsQUFBTTs7cUJBQWxCO0FBRGEsbUNBQUE7b0NBQUE7MkJBRUEsSUFGQSxBQUVBLEFBQUk7O3FCQUFqQjtBQUZhLG9DQUduQjs7NEJBQUEsQUFBUSxtQ0FBaUMsS0FBekMsQUFBOEMsQUFFOUM7OzBCQUFNLHlCQUxhLEFBS25CLEFBQU0sQUFBZTs7K0JBTEYsQUFNWixBQUNJO0FBREosQUFDSDs7cUJBUGU7cUJBQUE7b0NBQUE7O0FBQUE7Z0JBQUE7QUFBdkIsQUFVQTtrQkFBQSxBQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSIsImZpbGUiOiJTaWRlYmFyLmpzIiwic291cmNlUm9vdCI6IkQ6L3hhbXBwL2h0ZG9jcy9tb3ZldW5pdDIifQ==