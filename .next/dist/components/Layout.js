'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _antd = require('antd');

var _Header = require('./Header');

var _Header2 = _interopRequireDefault(_Header);

var _Nav = require('./Nav');

var _Nav2 = _interopRequireDefault(_Nav);

var _Sidebar = require('./Sidebar');

var _Sidebar2 = _interopRequireDefault(_Sidebar);

var _antd2 = require('antd/dist/antd.css');

var _antd3 = _interopRequireDefault(_antd2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = 'D:\\xampp\\htdocs\\moveunit2\\components\\Layout.js';

var SubMenu = _antd.Menu.SubMenu;
var MenuItemGroup = _antd.Menu.ItemGroup;

var Header = _antd.Layout.Header,
    Footer = _antd.Layout.Footer,
    Sider = _antd.Layout.Sider,
    Content = _antd.Layout.Content;

var MyLayout = function MyLayout(props) {
    return _react2.default.createElement('div', {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 12
        }
    }, _react2.default.createElement('style', { dangerouslySetInnerHTML: { __html: _antd3.default }, __source: {
            fileName: _jsxFileName,
            lineNumber: 13
        }
    }), _react2.default.createElement(_Header2.default, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 14
        }
    }), _react2.default.createElement(_antd.Layout, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 15
        }
    }, _react2.default.createElement(Header, { style: { background: 'none', padding: 0, height: "100%" }, __source: {
            fileName: _jsxFileName,
            lineNumber: 16
        }
    }, _react2.default.createElement(_Nav2.default, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 16
        }
    })), _react2.default.createElement(_antd.Layout, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 17
        }
    }, _react2.default.createElement(Content, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 18
        }
    }, props.children), _react2.default.createElement(Sider, { style: { background: "none" }, __source: {
            fileName: _jsxFileName,
            lineNumber: 19
        }
    }, _react2.default.createElement(_Sidebar2.default, {
        __source: {
            fileName: _jsxFileName,
            lineNumber: 19
        }
    })))));
};

exports.default = MyLayout;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXExheW91dC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJIZWFkIiwiTmF2IiwiSW5kZXgiLCJzdHlsZXNoZWV0IiwiTWVudSIsIkljb24iLCJCcmVhZGNydW1iIiwiU3ViTWVudSIsIk1lbnVJdGVtR3JvdXAiLCJJdGVtR3JvdXAiLCJIZWFkZXIiLCJGb290ZXIiLCJTaWRlciIsIkNvbnRlbnQiLCJNeUxheW91dCIsInByb3BzIiwiX19odG1sIiwiYmFja2dyb3VuZCIsInBhZGRpbmciLCJoZWlnaHQiLCJjaGlsZHJlbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLEFBQVM7O0FBQ1QsQUFBTyxBQUFVOzs7O0FBQ2pCLEFBQU8sQUFBUzs7OztBQUNoQixBQUFPLEFBQVc7Ozs7QUFDbEIsQUFBTyxBQUNQLEFBQVMsQUFBTSxBQUFNOzs7Ozs7OztBQUNyQixJQUFNLFVBQVUsV0FBaEIsQUFBcUI7QUFDckIsSUFBTSxnQkFBZ0IsV0FBdEIsQUFBMkI7O0lBRW5CLEEsU0FBbUMsQSxhQUFuQyxBO0lBQVEsQSxTQUEyQixBLGEsQUFBM0I7SSxBQUFRLFEsQUFBbUIsYUFBbkIsQTtJLEFBQU8sVSxBQUFZLGFBQVosQTs7QUFDL0IsSUFBTSxXQUFXLFNBQVgsQUFBVyxTQUFBLEFBQUMsT0FBRDsyQkFDYixjQUFBOztzQkFBQTt3QkFBQSxBQUNJO0FBREo7QUFBQSxLQUFBLDJDQUNXLHlCQUF5QixFQUFoQyxBQUFnQyxBQUFFLEFBQVE7c0JBQTFDO3dCQURKLEFBQ0ksQUFDQTtBQURBO3dCQUNBLEFBQUM7O3NCQUFEO3dCQUZKLEFBRUksQUFDQTtBQURBO0FBQUEsd0JBQ0EsQUFBQzs7c0JBQUQ7d0JBQUEsQUFDSTtBQURKO0FBQUEsdUJBQ0ssY0FBRCxVQUFRLE9BQU8sRUFBRSxZQUFGLEFBQWMsUUFBUSxTQUF0QixBQUErQixHQUFHLFFBQWpELEFBQWUsQUFBeUM7c0JBQXhEO3dCQUFBLEFBQWtFO0FBQWxFO3VCQUFrRSxBQUFDOztzQkFBRDt3QkFEdEUsQUFDSSxBQUFrRSxBQUNsRTtBQURrRTtBQUFBLHlCQUNsRSxBQUFDOztzQkFBRDt3QkFBQSxBQUNJO0FBREo7QUFBQSx1QkFDSyxjQUFEOztzQkFBQTt3QkFBQSxBQUFVO0FBQVY7QUFBQSxhQURKLEFBQ0ksQUFBZ0IsQUFDaEIsMkJBQUMsY0FBRCxTQUFPLE9BQU8sRUFBQyxZQUFmLEFBQWMsQUFBWTtzQkFBMUI7d0JBQUEsQUFBbUM7QUFBbkM7dUJBQW1DLEFBQUM7O3NCQUFEO3dCQVJsQyxBQUNiLEFBR0ksQUFFSSxBQUVJLEFBQW1DO0FBQUE7QUFBQTtBQVJuRCxBQWNBOztrQkFBQSxBQUFlIiwiZmlsZSI6IkxheW91dC5qcyIsInNvdXJjZVJvb3QiOiJEOi94YW1wcC9odGRvY3MvbW92ZXVuaXQyIn0=